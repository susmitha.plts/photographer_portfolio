<?php

use Illuminate\Database\Seeder;

class ProjectTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
 public function run()
    {
        DB::table('project_types')->truncate();
        DB::table('project_types')->insert(['name' => 'Photos']);
		DB::table('project_types')->insert(['name' => 'Videos']);  
    }
}
