<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 
		DB::table('users')->truncate();
		DB::table('users')->insert(['name'=>'admin','email' => 'admin@portfolio.com', 'password'=>'e10adc3949ba59abbe56e057f20f883e']);
    }
}
