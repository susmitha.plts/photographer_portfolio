<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('settings', function(Blueprint $table)
            {
                $table->integer('id', true);
                $table->enum('type', array('address','phone','skype','email'))->nullable();
                $table->string('key')->nullable();
                $table->text('value', 65535)->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
