<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {   
        Schema::create('photos', function(Blueprint $table)
            {
                $table->integer('id', true);
                $table->integer('project_id');
                $table->string('original_file')->nullable();
                $table->string('filename')->nullable();
                $table->text('description', 65535)->nullable();
                $table->timestamps();
                
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photos');
    }
}
