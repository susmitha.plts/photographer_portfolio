<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('contents', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('key')->nullable();
            $table->mediumText('title')->nullable();
            $table->string('meta_title')->nullable();
            $table->mediumText('keywords')->nullable();
            $table->mediumText('meta_description')->nullable();
            $table->longText('content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('contents');
    }
}
