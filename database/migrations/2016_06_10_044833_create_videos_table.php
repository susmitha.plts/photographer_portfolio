<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('videos', function(Blueprint $table)
            {
                $table->integer('id', true);
                $table->integer('project_id');
                $table->string('title')->nullable();
                $table->text('description', 65535)->nullable();
                $table->string('url');
                $table->timestamps();

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }
}
