<?php 
use Carbon\Carbon;
use Image;
/* Method to create thumbnail for user cargo picture 19-08-2015*/
function resizeAndThumbnailPhoto($image_name){

	// getting paths from custom configuration
	$upload_path 			= config('custom.photos_folder');
	$upload_thumb_path 		= config('custom.photos_thumbnail_folder');
	$full_image_path 		= trim($upload_path . $image_name);
	$full_thumb_image_path 	= trim($upload_thumb_path . $image_name);
	//echo $full_thumb_image_path;exit;
	// resizing an uploaded file
	$image = \Image::make($full_image_path);
	// add callback functionality to retain maximal original image size
	/*$image->fit(config('custom.image_user_cargo_full_width'), config('custom.image_user_cargo_full_height'), function ($constraint) {
		$constraint->upsize();
	});*/


    //echo "innnnnn";exit;
	$image->save();
	// make thumbnails for fast loading	
	$image->fit(config('custom.photos_thumbnail_width'), config('custom.photos_thumbnail_height'), function ($constraint) {
		$constraint->upsize();
	});
	$image->save($full_thumb_image_path);
	return true;
}
/* Method to set new name for image */
function setNewNameForImage($image_original_name){
	// remove extension
	$without_ext = removeExtension($image_original_name);
	//return $without_ext;
	return str_slug($without_ext, '-');
}
/* remove extension from file name */
function removeExtension($original_name){
	return preg_replace('/\\.[^.\\s]{3,4}$/', '', $original_name);
}
/* Generate random number or string */
function generateRandomString($prefix = "", $alpha_numeric = 0, $count = 10){
	if($alpha_numeric = 0){
		$characters = '123456789';
	}else if($alpha_numeric = 1){
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	}else{
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
	}
	// generate a pin based on 2 * 7 digits + a random character
	$pin = mt_rand(1000000, 9999999) . mt_rand(1000000, 9999999) . $characters[rand(0, strlen($characters) - 1)];
	// shuffle the result
	$string = str_shuffle($pin);
	if(!empty($prefix)){
		$string = $prefix . "-" . $string;
	}
	$string = substr($string,0, $count);
	return trim($string);
}
function pr($message) {
	echo "<pre>";
	print_r($message);
	echo "</pre>";
}
function deleteFile($files){
	\File::delete($files);
	return true;
}
?>