<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::controllers([
   'auth' => 'Auth\AuthController',
   'password' => 'Auth\PasswordController',
]);
Route::get('/', function () {
    return view('welcome');
});


Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
# Admin login page
Route::get('admin', array('as' => 'admindefault', 'uses' => 'HomeController@login'));
Route::get('admin/login', array('as' => 'adminlogin', 'uses' => 'HomeController@login'));
Route::post('admin/dologin', array('as' => 'adminloginpost', 'uses' => 'HomeController@doLogin'));
# project view in front end
Route::get('project/{id}/view',array('as'=>'frontendprojectview','uses' => 'HomeController@Homeprojectview'));

Route::get('homein',array('as'=>'home','uses' => 'HomeController@home'));
# photo project listing in front end
Route::get('photos',array('as'=>'home','uses' => 'HomeController@photos'));
# video project listing in front end
Route::get('videos',array('as'=>'home','uses' => 'HomeController@videos'));
Route::group([ 'prefix' => 'admin' ,'middleware' => 'App\Http\Middleware\AdminMiddleware'], function() {

   # admin logout
   Route::get('logout', array('as' => 'adminlogout', 'uses' => 'HomeController@getLogout'));
   # admin dashboard
   Route::get('dashboard', array('as' => 'adminhome', 'uses' => 'HomeController@adminIndex'));
   # admin about us page
   Route::get('about', array('as' => 'adminabout', 'uses' => 'HomeController@adminaboutUs'));
   # admin about us update
   Route::post('about/{id}/update', array('as' => 'adminaboutupdate', 'uses' => 'HomeController@adminaboutUpdate'));
   # admin about us add
   Route::post('about/addpost', array('as' => 'adminaboutadd', 'uses' => 'HomeController@adminaboutadd'));
   # admin contact us index page
   Route::get('contact/index', array('as' => 'admincontactindex' , 'uses' =>'HomeController@adminContactIndex'));
   # admin contact us edit page
   Route::get('contact/{id}/edit', array('as' => 'admincontactedit', 'uses' => 'HomeController@contactEdit'));
   # admin contact us add page
   Route::get('contact/add', array('as' => 'admincontactadd' , 'uses' =>'HomeController@addContact'));
   # admin contact us add
   Route::post('contact/addpost', array('as' => 'admincontactaddpost' , 'uses' =>'HomeController@admincontactaddPost'));
   # admin contact us update
   Route::post('contact/update', array('as' => 'admincontactupdate' , 'uses' =>'HomeController@contactUpdate'));
   # admin contact us delete
   Route::any('contact/{id}/delete', array('as' => 'admincontactdestroy', 'uses' => 'HomeController@adminContactDestroy'));
   # admin to list all projects
   Route::get('project/index', array('as' => 'adminprojectindex', 'uses' => 'HomeController@projectIndex'));
   # admin to add new project
   Route::any('project/add', array('as' => 'adminprojectadd', 'uses' => 'HomeController@adminProjectAdd'));
   Route::any('project/{id}/add', array('as' => 'adminprojectaddpo', 'uses' => 'HomeController@adminProjectAddPo'));
   # admin to add photo project
   Route::post('project/photo/addpost',array('as' => 'photoprojectaddpost' , 'uses' => 'HomeController@adminPhotoprojectAddPost'));
   # admin to delete project
   Route::any('project/{id}/delete', array('as' => 'adminprojectdestroy', 'uses' => 'HomeController@adminProjectDestroy'));
   # admin to edit  project
   Route::get('project/{id}/edit', array('as' => 'adminprojectedit', 'uses' => 'HomeController@projectEdit'));
   # photos listing in photo project in admin
   Route::get('project/photos/index',array('as' => 'projectphotosindex' , 'uses' => 'HomeController@projectPhotosIndex'));
   # admin to delete photo in a photo project
   Route::any('project/photos/{id}/delete', array('as' => 'adminphotodestroy', 'uses' => 'HomeController@adminPhotoDestroy'));
   # admin to edit a photo in a photo project
   Route::any('project/photos/{id}/edit', array('as' => 'adminprojectphotoedit', 'uses' => 'HomeController@adminPhotoProjectEdit'));
   # admin to update a photo in a photo project
   Route::post('project/photo/update' , array('as' => 'adminphotoupdate' ,'uses' => 'HomeController@photoupdate'));
   # admin to list photos in a photo project
   Route::any('project/photo/{id}/index',array('as' => 'adminphotoprojectindex' , 'uses' =>'HomeController@PhotoprojectIndex'));
   # admin to edit a photo project
   Route::get('project/photos/{id}/edit_index',array('as' => 'adminphotoprojecteditindex' , 'uses' => 'HomeController@photoProjecteditIndex'));
   # admin to add more photos to a photo project
   Route::post('project/photo/addmore',array('as' => 'adminphotoprojectaddmore' , 'uses' => 'HomeController@photoprojectaddMore'));
   # admin to update a photo project
   Route::post('project/update',array('as' => 'adminphotoprojectupdate' , 'uses' => 'HomeController@projectUpdate'));
   # admin to list photo projects
   Route::get('project/photo/index',array('as' => 'photoprojectindex' , 'uses' => 'HomeController@PhotoprojectsIndex'));
   # admin to list video projects
   Route::get('project/video/index',array('as' => 'videoprojectindex' , 'uses' => 'HomeController@VideoprojectsIndex'));
   # admin to view projects
   Route::get('projects/{id}/view',array('as' => 'projectview' ,'uses' => 'HomeController@projectView'));
   # admin to view photo projects
   Route::get('projects/photos/{id}/view',array('as' => 'photoprojectview' ,'uses' => 'HomeController@PhotoprojectView'));
   # admin to view video projects
   Route::get('projects/videos/{id}/view',array('as' => 'videoprojectview' ,'uses' => 'HomeController@VideoprojectView'));
   # admin to add video projects
   Route::post('project/video/addpost',array('as' => 'videoprojectaddpost' ,'uses' => 'HomeController@videoProjectaddPost'));
   # admin to list videos in video projects
   Route::get('project/video/{id}/index' ,array('as' =>'videoprojectvideosindex' , 'uses' => 'HomeController@VideoProjectIndex'));
   # admin to edit video in video projects
   Route::get('project/video/{id}/edit' ,array('as' =>'videoprojectvideosedit' , 'uses' => 'HomeController@VideoProjectEdit'));
   # admin to view videos in video projects
   Route::get('project/video/{id}/view' ,array('as' =>'videoprojectvideosview' , 'uses' => 'HomeController@VideoProjectView'));
   # admin to edit a video in video projects
   Route::post('project/video/update' , array('as' => 'adminvideoupdate' ,'uses' => 'HomeController@videoupdate'));
   //Route::any('project/{id}/delete', array('as' => 'adminprojectdestroy', 'uses' => 'HomeController@adminProjectDestroy'));
   # admin to delete a video in video projects
   Route::any('project/videos/{id}/delete', array('as' => 'adminvideodestroy', 'uses' => 'HomeController@adminVideoDestroy'));
   # admin to view video project
   Route::get('projects/videos/{id}/view',array('as' => 'videoprojectview' ,'uses' => 'HomeController@VideoprojectView'));
   # admin to add more videos to an existing video project
   Route::post('project/video/addmore',array('as' => 'videoaddmoreupdate' ,'uses' => 'HomeController@addmorevideos'));
   # admin to edit videos in an existing video project
   Route::get('project/videos/{id}/edit_index',array('as' => 'videoprojectalledit' ,'uses' =>'HomeController@videoprojecteditall'));
});


