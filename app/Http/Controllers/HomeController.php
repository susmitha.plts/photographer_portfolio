<?php namespace App\Http\Controllers;
use Session;
use Redirect;
use Auth;
use DB;
use Input;
use App\Setting;
use App\Content;
use Validator;
use App\Project;
use App\Photo;
use App\Video;
use Image;
use Hash;
class HomeController extends Controller
{     
    /*
    * Function to load home page in front end
    */ 
    public function index(){

        $content   = Content::select('content')->where('key','about_us')->first();
        $address   = Setting::select('value')->where('key','address')->first();
        $email     = Setting::select('value')->where('key','email')->first();
        $phone     = Setting::select('value')->where('key','phone')->first();
        $skype     = Setting::select('value')->where('key','skype_id')->first();
        $projects  = Project::orderBy('id','desc')->get();

        return view('frontend.home',['content' => $content , 'address' => $address ,'email' => $email,'phone' =>$phone, 'skype' => $skype,'projects' => $projects]);
    }

    /*
    * Function to load admin dashboard
    */ 
    public function adminIndex(){
        return view('admin.dashboard');
    }

    /*
    * Function to load admin login page
    */ 
    public function login(){
        return view('admin.login');
    }
    /*
    * Function for admin login
    */ 
    public function doLogin(){

        $input    = Input::all();
        $validate = Validator::make(Input::all(), [
          'username' => 'required',
          'password' => 'required'
          ]);
        if(!$validate->fails()){
            //$hs = Hash::make('123456');
            $userdata = array(
                'email'     => Input::get('username'),
                'password'  => Input::get('password')
                );
            if (Auth::attempt($userdata)) {
                $user = Auth::user();
                return redirect()->intended('admin/dashboard');
            }else{
                return Redirect::back()->with('error', 'Incorrect username or password.');
            }
        }else{
            return Redirect::back()->with('error', 'Incorrect username or password.');
        }
        
    }

    /*
    * Function to load about us page in admin
    */ 

    public function adminaboutUs(){
        $content   = Content::where('key','about_us')->first();
        if($content){
            return view('admin.about',['content' => $content]);
        }else{
            return view('admin.about_add')->with('error', "You haven't created an about us page yet!");
        }
    }

    /*
    * Function to update about us in admin
    */ 

    public function adminaboutUpdate($id){
        $input = Input::all();
        if(!empty($input)){
            $validate = Validator::make(Input::all(), [
                'title'   => 'required',
                'content' => 'required'
                ]);

            if (!$validate->fails()){
                $content 	                     = Content::findOrFail($id);
                $content->title				     = 	Input::get('title');
                $content->meta_title		     = 	Input::get('meta_title');
                $content->keywords			     = 	Input::get('keywords');
                $content->meta_description	     = 	Input::get('meta_description');
                $content->content 			     = 	Input::get('content');

                if($content->save()){
                  return redirect("admin/about")->with('success', 'Content updated!');	
              }
          }else{
            return Redirect::back()->withErrors($validate)->withInput();
        }
    }else{
        return Redirect::back()->with('error', 'Invalid request');
    }
}

    /*
    * Function to add about us in admin
    */ 
    public function adminaboutadd(){
        $input = Input::all();

        if(!empty($input)){
            $validate = Validator::make(Input::all(), [
             'title' => 'required',
             'content' => 'required'
             ]);

            if (!$validate->fails()){
              $content  = new Content;
              $content->title             =   Input::get('title');
              $content->key               =   "about_us";
              $content->meta_title        =   Input::get('meta_title');
              $content->keywords          =   Input::get('keywords');
              $content->meta_description  =   Input::get('meta_description');
              $content->content           =   Input::get('content');

              if($content->save()){
                return redirect("admin/about")->with('success', 'added successfully!'); 
            }
        }else{
          return Redirect::back()->withErrors($validate)->withInput();
      }
  }else{
    return Redirect::back()->with('error', 'Invalid request');
}
}

    /*
     * Function to list contact us details in admin
    */ 
    public function adminContactIndex(){
      $settings =  Setting::orderBy('id','asc')
      ->select('settings.id', 'settings.type', 'settings.key', 'settings.value','settings.created_at')
      ->paginate(15);
      return view('admin.contactus', ['settings' => $settings]);
  }
    /*
     * Function to add contact us details in admin
    */ 
    public function addContact(){
      $settings_types = config('custom.settings_type');
      return view('admin/contact_add',['settings_types'=> $settings_types]);
  }
    /*
     * Function to load edit page for  contact us in admin
    */ 
    public function contactEdit($id){
      $setting = Setting::find($id);
      if($setting){

        $settings_types = config('custom.settings_type');
        return view('admin/contact_edit', ['settings_types' => $settings_types, 'setting' => $setting]);

    }else{
        return redirect::back()->with('error','Invalid Request');
    }
}

    /*
     * Function to update contact us details in admin
    */ 

    public function contactUpdate(){
      $input = Input::all();

        if(!empty($input)){
            $validate = Validator::make(Input::all(), [
              'settings_type' => 'required',
              'settings_value' => 'required'
            ]);
            if (!$validate->fails()){
                $id                 = Input::get('id');
                $settings           = Setting::findOrFail($id);
                if($settings){
                    $settings->type     = Input::get('settings_type');
                    $settings->value    = Input::get('settings_value');
                    $settings->save();
                    return redirect("admin/contact/index")->with('success', 'Contact details updated successfully!');
                }else{
                    return Redirect::back()->with('error', 'Invalid request');
                }
            }else{
                return Redirect::back()->withErrors($validate);
            }
        }else{
            return Redirect::back()->with('error', 'Invalid request');
        }
    }

    /*
     * Function to add contact us details in admin
    */ 

    public function admincontactaddPost(){
        $input = Input::all();
        if(!empty($input)){
            $validate = Validator::make(Input::all(), [
              'settings_type' => 'required',
              'key' => 'required',
              'settings_value' => 'required'
            ]);
            if (!$validate->fails()){
                $settings         = new Setting;
                $settings->type   = Input::get('settings_type');
                $settings->key    = Input::get('key');
                $settings->value  = Input::get('settings_value');
                $settings->save();
                return redirect("admin/contact/index")->with('success', 'Contact details added successfully!');
            }else{
                return Redirect::back()->withErrors($validate);
            }
        }else{
            return Redirect::back()->with('error', 'Invalid request');
        }
    }

    /*
     * Function to destroy contact us details in admin
    */ 
    public function adminContactDestroy($id){
        $setting = Setting::find($id);
        if($setting){
            Setting::destroy($id);
            return Redirect::to('admin/contact/index')->with('success', 'Contact detail deleted successfully!');
        }else {
            return Redirect::to('admin/contact/index')->with('error', 'You cannot delete!');
        }
    }

    /*
     * Function to list all projects in admin
    */ 

    public function projectIndex(){
        $projects =  Project::select('projects.id','projects.type_id','projects.project_name','projects.description','projects.updated_at','project_types.name')
                  ->join('project_types','projects.type_id','=','project_types.id')
                  ->paginate(15);
        return view('admin/projects_index',['projects' => $projects]);
    }

    /*
     * Function to load page for project add in admin
    */ 

    public function adminProjectAdd(){
        return view('admin.project_add');
    }

    /*
     * Function to select project type for adding project in admin
    */ 

    public function adminProjectAddPo($id){
        if($id == 1){
            return view('admin.project_photos_add',['id' => $id]);
        }elseif($id == 2){
            return view('admin.project_videos_add',['id' => $id]);
        }
    }

    /*
     * Function to add photo project in admin
    */ 

    public function adminPhotoprojectAddPost(){

        $input = Input::all();
        if(!empty($input)){
            $project_title          = Input::get('title');
            $project_description    = Input::get('description');
            $project                = new Project;
            $project->project_name  = $project_title;
            $project->description   = $project_description;
            $project->type_id       = Input::get('type_id');
            $project->save();
            if($project->save()){
                $pro_id = $project->id;
                    if(Input::hasFile('files')) {  
                        $upload_doc_path      = config('custom.photos_folder');
                        $upload_thumb_path    = config('custom.photos_thumbnail_folder');
                        foreach (array_filter(Input::file('files')) as $key => $p_file) {
                            if ($p_file->isValid()) {
                                $extension    = $p_file->getClientOriginalExtension(); 
                                $orginal_name = $p_file->getClientOriginalName();
                                $fileName     = setNewNameForImage($orginal_name) . '.' .generateRandomString(2, 15) . '.'. $extension;
                                // uploading file to given path
                                $p_file->move($upload_doc_path, $fileName);
                                resizeAndThumbnailPhoto($fileName);

                                $photo                  = new Photo;
                                $photo->project_id      = $project->id;
                                $photo->original_file   = $p_file->getClientOriginalName();
                                $photo->filename        = $fileName;
                                $photo->save();
                            }
                        }      
                    }
                }
            $photos = "";
            return Redirect::to('admin/project/photo/'.$pro_id.'/index')->with('success', 'Added successfully! edit photos now !!');
        }else{
            return Redirect::back()->with('error', 'Invalid request');
        }
    }

    /*
     * Function to add photo project in admin
    */ 

    public function adminPhotoDestroy($id){
        $photo                = Photo::findOrFail($id);
        $project_id           = $photo->project_id;

        $upload_path          = config('custom.photos_folder');
        $upload_thumb_path    = config('custom.photos_thumbnail_folder');
        if($photo){
            $old_photo = $photo->filename;
            if(!empty($old_photo)){
                $files = array(trim($upload_path . $old_photo), trim($upload_thumb_path . $old_photo));
                // delete file/files helper
                deleteFile($files);
            }
            Photo::where('id',$id)->delete();
            return Redirect::to('admin/project/photo/'.$project_id.'/index')->with('success', 'Deleted successfully!!');
        }else{
            return Redirect::back()->with('error', 'Invalid Request!!');
        }
    }
    
    /*
     * Function to edit photo project in admin
    */

    public function adminPhotoProjectEdit($id){

        $photo = Photo::select('photos.id','photos.original_file','photos.description','photos.title','projects.project_name','photos.filename','photos.updated_at','photos.project_id')
                  ->join('projects','projects.id','=','photos.project_id')
                  ->where('photos.id',$id)->first();
        if($photo){
            return view('admin/photos_edit',['photo' => $photo]);
        }else{
            return Redirect::back()->with('error', 'Invalid request');
        }
    }
    
    /*
     * Function to update photo project in admin
    */

    public function photoupdate(){
        $input = Input::all();
        if(!empty($input)){
            $validate = Validator::make(Input::all(), [
              'title' => 'required',
              'description' => 'required'
            ]);
            if (!$validate->fails()){
                $id                 = Input::get('photo_id');
                $change_photo       = Input::get('files');
                $photo              = Photo::findOrFail($id);
                $upload_path        = config('custom.photos_folder');
                $upload_thumb_path  = config('custom.photos_thumbnail_folder');
                if($photo){
                    if(Input::hasFile('files')) {  
                        $old_photo = $photo->filename;
                        if(!empty($old_photo)){
                            $files = array(trim($upload_path . $old_photo), trim($upload_thumb_path . $old_photo));
                            // delete file/files helper
                            deleteFile($files);
                        }
                        foreach (array_filter(Input::file('files')) as $key => $p_file) {
                            if ($p_file->isValid()) {
                                $extension    = $p_file->getClientOriginalExtension(); 
                                $orginal_name = $p_file->getClientOriginalName();
                                  //echo $orginal_name;exit;
                                $fileName = setNewNameForImage($orginal_name) . '.' .generateRandomString(2, 15) . '.'. $extension;
                                    // uploading file to given path
                                $p_file->move($upload_path, $fileName);
                                resizeAndThumbnailPhoto($fileName);
                                $photo->original_file     = $p_file->getClientOriginalName();
                                $photo->filename          = $fileName;
                                $photo->save();
                            }
                        }
                    }
                    $photo->title          = Input::get('title');
                    $photo->description    = Input::get('description');
                    $photo->save();
                    $project_id = Input::get('project_id');
                    return Redirect::to('admin/project/photo/'.$project_id.'/index')->with('success', 'Details updated successfully!!');
                }else{
                    return Redirect::back()->with('error', 'Invalid request');
                }
            }else{
                return Redirect::back()->withErrors($validate);
            }
        }else{
            return Redirect::back()->with('error', 'Invalid request');
        }
    }

    /*
     * Function to add list photo projects in admin
    */

    public function PhotoprojectIndex($id){
        $photos = Photo::select('photos.id','photos.original_file','photos.description','photos.title','projects.project_name','photos.filename','photos.updated_at')
                    ->join('projects','projects.id','=','photos.project_id')
                    ->where('photos.project_id',$id)
                    ->paginate(15);
        return view('admin/photos_index',['photos' => $photos,'project_id' => $id]);
    }

    /*
     * Function to delete a project by admin
    */
    public function adminProjectDestroy($id){
        $project = Project::findOrFail($id);
        if($project){
            $type_id = $project->type_id;
            $upload_path          = config('custom.photos_folder');
            $upload_thumb_path    = config('custom.photos_thumbnail_folder');
            if($type_id == 1){
                $photos = Photo::where('project_id',$id)->get();
                foreach($photos as $photo){
                    $old_photo            = $photo->filename;
                    if(!empty($old_photo)){
                        $files = array(trim($upload_path . $old_photo), trim($upload_thumb_path . $old_photo));
                        // delete file/files helper
                        deleteFile($files);
                    }
                    Photo::where('id',$photo->id)->delete();
                }
            }elseif($type_id == 2){
                $videos = Video::where('project_id',$id)->get();
                foreach($videos as $video){
                    Video::where('id',$video->id)->delete();
                }
            }
            Project::where('id',$id)->delete();
            return Redirect::to('admin/project/index')->with('success', 'Deleted successfully!!');
        }else{
            return Redirect::back()->with('error', 'Invalid Request!!');
        }
    }
    /*
     * Function to edit project by admin
    */
    public function projectEdit($id){
        $project = Project::where('id',$id)->first();
        return view('admin.project_photos_edit',['project' => $project]);
    }
    
    /*
     * Function to edit photos in photo project by admin
    */

    public function photoProjecteditIndex($project_id){
 
        $input = Input::all();
        $photos = "";
        if(!empty($project_id)){
            $photos = Photo::select('photos.id','photos.original_file','photos.description','photos.title','projects.project_name','photos.filename','photos.updated_at')
                    ->join('projects','projects.id','=','photos.project_id')
                    ->where('photos.project_id',$project_id)
                    ->paginate(15);
            return view('admin.photo_projects_edit_index',['photos' => $photos , 'project_id' => $project_id]);
        }else{
            return Redirect::back()->with('error', 'Invalid Request!!');
        }
    }
    
    /*
     * Function to add more photo to a photo project by admin
    */

    public function photoprojectaddMore(){
        $project_id = Input::get('project_id');
        if(!empty($project_id)){
            if(Input::hasFile('files')) {  
                $upload_doc_path = config('custom.photos_folder');
                $upload_thumb_path    = config('custom.photos_thumbnail_folder');
                foreach (array_filter(Input::file('files')) as $key => $p_file) {
                    if ($p_file->isValid()) {

                        $extension    = $p_file->getClientOriginalExtension(); 
                        $orginal_name = $p_file->getClientOriginalName();
                        $fileName = setNewNameForImage($orginal_name) . '.' .generateRandomString(2, 15) . '.'. $extension;
                        $p_file->move($upload_doc_path, $fileName);
                        resizeAndThumbnailPhoto($fileName);
                        $photo                    = new Photo;
                        $photo->project_id        = $project_id;
                        $photo->original_file     = $p_file->getClientOriginalName();
                        $photo->filename          = $fileName;
                        $photo->save();

                    }
                }
                return Redirect::to('admin/project/photo/'.$project_id.'/index')->with('success', 'Added successfully!!');      
            }else{
                return Redirect::back()->with('error', 'No new files selected to add!!');
            }
        }else{
            return Redirect::back()->with('error', 'Invalid Request!!');
        }
    }

    /*
     * Function to update a  project by admin
    */

    public function projectUpdate(){

        $input = Input::all();
        $id = Input::get('pro_id');
        $title = Input::get('title');
        $description = Input::get('description');
        $project = Input::get('submit_pro');
        $photo_pro = Input::get('submit_photo');
        $video_pro = Input::get('submit_video');
        if(isset($photo_pro)){
            if(!empty($title)){
                $project = Project::findOrFail($id);
                if($project){
                    $project->project_name = Input::get('title');
                    $project->save();
                }
            }
            if(!empty($description)){
              $project = Project::findOrFail($id);
                if($project){
                    $project->description = Input::get('description');
                    $project->save();
                }
            }
            return Redirect::to('admin/project/photos/'.$id.'/edit_index')->with('success','updated successfully');
        }elseif(isset($video_pro)){
            if(!empty($title)){
                $project = Project::findOrFail($id);
                if($project){
                    $project->project_name = Input::get('title');
                    $project->save();
                }
            }
            if(!empty($description)){
                $project = Project::findOrFail($id);
                if($project){
                    $project->description = Input::get('description');
                    $project->save();
                }
            }

            return Redirect::to('admin/project/videos/'.$id.'/edit_index')->with('success','updated successfully');
        }elseif(isset($project)){
            if(!empty($title)){
                $project = Project::findOrFail($id);
                if($project){
                    $project->project_name = Input::get('title');
                    $project->save();
                }
            }
            if(!empty($description)){
                $project = Project::findOrFail($id);
                if($project){
                    $project->description = Input::get('description');
                    $project->save();
                }
            }
            return Redirect::to('admin/project/index')->with('success','updated successfully');
        }
    }

    /*
     * Function to list all photo projects in admin
    */

    public function PhotoprojectsIndex(){
        $projects = Project::where('type_id',1)
            ->paginate(15);
        if(!empty($projects)){
            return view('admin.photo_projects_index',['projects' => $projects]);
        }else{
            return Redirect::back()->with('error', 'No projects available!!');
        }
        
    }

    /*
     * Function to list all  video projects in admin
    */
    
    public function VideoprojectsIndex(){
        $projects = Project::where('type_id',2)
                       ->paginate(15);                   
        if($projects){
            return view('admin.video_projects_index',['projects' => $projects]);
        }else{
            return Redirect::back()->with('error', 'No projects available!!');
        }
    }
    
    /*
     * Function to view project in admin
    */

    public function projectView($id){

        $project = Project::where('id',$id)->first();
        if(!empty($project)){
            return view('admin.project_view',['project' => $project]);
        }else{
            return Redirect::back()->with('error', 'Selected project not available!!');
        }
    }
    
    /*
     * Function to get logout
    */

    public function getLogout() {
        Auth::logout();
        return Redirect::to('admin/login');
    }
    
    /*
     * Function to view photo in  photo projects in admin
    */

    public function PhotoprojectView($id){
        $photo = Photo::where('id',$id)->first();
        if(!empty($photo)){
            return view('admin.photo_project_view',['photo' => $photo]);
        }else{
            return Redirect::back()->with('error', 'Selected project not available!!');
        }
    }
    
    /*
     * Function to view video in  video projects in admin
    */

    public function VideoprojectView($id){
        $video = Video::where('id',$id)->first();
        if(!empty($video)){
            return view('admin.video_project_view',['video' => $video]);
        }else{
            return Redirect::back()->with('error', 'Selected project not available!!');
        }
    }
    
    /*
     * Function to view add video projects in admin
    */

    public function videoProjectaddPost(){

        $input = Input::all();
        $links = Input::get('video_links');
        if(!empty($input)){
            $project_title = Input::get('title');
            $project_description = Input::get('description');
            $project = new Project;
            $project->project_name = $project_title;
            $project->description = $project_description;
            $project->type_id = Input::get('type_id');
            $project->save();
            if($project->save()){
                $pro_id = $project->id;
                foreach($links as $link){
                    $video                  = new Video;
                    $video->project_id      = $project->id;
                    $video->url             = trim($link);
                    $video->save();
                }
            }
        }      
        $videos = "";
        return Redirect::to('admin/project/video/'.$pro_id.'/index')->with('success', 'Added successfully! edit videos now !!');
    }
    
    /*
     * admin to list videos in video projects
    */

    public function VideoProjectIndex($id){
        $videos = "";
        $videos = Video::select('videos.id','videos.description','videos.title','projects.project_name','videos.updated_at','videos.url')
                  ->join('projects','projects.id','=','videos.project_id')
                  ->where('videos.project_id',$id)
                  ->paginate(15);
        return view('admin/videos_index',['videos' => $videos,'project_id' => $id]);
    }

    /*
     * admin to edit videos by admin
    */

    public function VideoProjectEdit($id){
        $video = Video::select('videos.id','videos.description','videos.title','projects.project_name','videos.updated_at','videos.project_id','videos.url')
                    ->join('projects','projects.id','=','videos.project_id')
                    ->where('videos.id',$id)->first();
        if($video){
            return view('admin/videos_edit',['video' => $video]);
        }else{
            return Redirect::back()->with('error', 'Invalid request');
        }
    }

    /*
     * function to update video by admin
    */

    public function videoupdate(){
        $input = Input::all();
        if(!empty($input)){
            $validate = Validator::make(Input::all(), [
              'title' => 'required',
              'description' => 'required'
              ]);
            if (!$validate->fails()){
                $id                 = Input::get('video_id');
                $video              = Video::findOrFail($id);
                $change_video_url   = Input::get('change_video');
                if($video){
                    if(!empty($change_video_url)){
                        $video->url = trim($change_video_url);
                    }
                    $video->title          = Input::get('title');
                    $video->description    = Input::get('description');
                    $video->save();
                    $project_id = Input::get('project_id');
                    return Redirect::to('admin/project/video/'.$project_id.'/index')->with('success', 'Details updated successfully!!');

                }else{
                    return Redirect::back()->with('error', 'Invalid request');
                }
            }else{
                return Redirect::back()->withErrors($validate);
            }

        }else{
            return Redirect::back()->with('error', 'Invalid request');
        }
    }

    /*
     * admin to destroy video in video project
    */

    public function adminVideoDestroy($id){
        $video = Video::findOrFail($id);
        if(!empty($video)){
            $project_id = $video->project_id;
            Video::where('id',$id)->delete();
            return Redirect::to('admin/project/video/'.$project_id.'/index')->with('success', 'Deleted successfully!!');
        }else{
            return Redirect::back()->with('error', 'Invalid Request!!');
        }
    }
    
    /*
     * function to view a project in frontend
    */

    public function Homeprojectview($id){
        $project = Project::where('id',$id)->first();
        if($project){
            if($project->type_id == 1){
                $content   = Content::select('content')->where('key','about_us')->first();
                $address   = Setting::select('value')->where('key','address')->first();
                $email     = Setting::select('value')->where('key','email')->first();
                $phone     = Setting::select('value')->where('key','phone')->first();
                $skype     = Setting::select('value')->where('key','skype_id')->first();
                return view('frontend.photo_project_view',['project' => $project ,'content' => $content , 'address' => $address ,'email' => $email,'phone' =>$phone, 'skype' => $skype]);
            }else if($project->type_id == 2){
                $content   = Content::select('content')->where('key','about_us')->first();
                $address   = Setting::select('value')->where('key','address')->first();
                $email     = Setting::select('value')->where('key','email')->first();
                $phone     = Setting::select('value')->where('key','phone')->first();
                $skype     = Setting::select('value')->where('key','skype_id')->first();
                return view('frontend.video_project_view',['project' => $project ,'content' => $content , 'address' => $address ,'email' => $email,'phone' =>$phone, 'skype' => $skype]);
            }
        }
    }
    
    /*
     * function to list all projects in frontend
    */

    public function home(){
        $content   = Content::select('content')->where('key','about_us')->first();
        $address   = Setting::select('value')->where('key','address')->first();
        $email     = Setting::select('value')->where('key','email')->first();
        $phone     = Setting::select('value')->where('key','phone')->first();
        $skype     = Setting::select('value')->where('key','skype_id')->first();
        $projects  = Project::orderBy('id','desc')->get();
        return view('frontend.home',['content' => $content , 'address' => $address ,'email' => $email,'phone' =>$phone, 'skype' => $skype,'projects' => $projects]);
    }
    
    /*
     * function to list all photo projects in frontend
    */

    public function photos(){
        $content   = Content::select('content')->where('key','about_us')->first();
        $address   = Setting::select('value')->where('key','address')->first();
        $email     = Setting::select('value')->where('key','email')->first();
        $phone     = Setting::select('value')->where('key','phone')->first();
        $skype     = Setting::select('value')->where('key','skype_id')->first();
        $projects  = Project::where('type_id',1)->orderBy('id','desc')->get();

        return view('frontend.photo_projects',['content' => $content , 'address' => $address ,'email' => $email,'phone' =>$phone, 'skype' => $skype,'projects' => $projects]);
    }
    
    /*
     * function to list all video projects in frontend
    */

    public function videos(){

        $content   = Content::select('content')->where('key','about_us')->first();
        $address   = Setting::select('value')->where('key','address')->first();
        $email     = Setting::select('value')->where('key','email')->first();
        $phone     = Setting::select('value')->where('key','phone')->first();
        $skype     = Setting::select('value')->where('key','skype_id')->first();
        $projects  = Project::where('type_id',2)->orderBy('id','desc')->get();
        return view('frontend.video_projects',['content' => $content , 'address' => $address ,'email' => $email,'phone' =>$phone, 'skype' => $skype,'projects' => $projects]);
    }

    /*
     * function to add more videos to an existing video project in admin
    */

    public function addmorevideos(){
        $project_id = Input::get('project_id');
        $video_links = Input::get('video_links');
        if(!empty($project_id)){
            if(!empty($video_links)){
                foreach($video_links as $link){
                    $video                  = new Video;
                    $video->project_id      = $project_id;
                    $video->url             = trim($link);
                    $video->save();
                }
                return Redirect::to('admin/project/video/'.$project_id.'/index')->with('success', 'Added successfully!!');   
            }else{
                return Redirect::back()->with('error', 'No links provided to add!!');
            }   
        }else{
            return Redirect::back()->with('error', 'Invalid Request!!');
        }
    }
    
    /*
     * function to edit videos in a video project
    */

    public function videoprojecteditall($project_id){
        
        $input = Input::all();
        $photos = "";
            if(!empty($project_id)){
                $videos = "";
                $videos = Video::select('videos.id','videos.description','videos.title','projects.project_name','videos.updated_at','videos.url')
                        ->join('projects','projects.id','=','videos.project_id')
                        ->where('videos.project_id',$project_id)
                        ->paginate(15);
                return view('admin.video_projects_edit_index',['videos' => $videos , 'project_id' => $project_id]);
        }else{
            return Redirect::back()->with('error', 'Invalid Request!!');
        }
    }
}
