<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function photos(){

    	return $this->hasMany('App\Photo');
    }

    public function videos(){

    	return $this->hasMany('App\Video');
    }

    public function project_type(){

    	return $this->belongsTo('App\ProjectType', 'type_id');
    }
}
