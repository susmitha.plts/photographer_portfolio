<nav id="menu">
            <ul id="menu-nav">
                <li><a href="{{ url('home')}}">Home</a></li>
                <li><a href="#work">Our Work</a></li>
                <li><a href="#about">About Us</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="#" class="external">Shortcodes</a></li>
            </ul>
        </nav>