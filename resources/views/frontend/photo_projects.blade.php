@extends('frontend.index')
@section('content')
    <div class="span9">
        <div class="row">
            <section id="projects">
                @if(!empty($projects))
                    <ul id="thumbs">
                       @foreach($projects as $project)
                            <li class="item-thumbs span3 ">
                                <a class="hover-wrap fancybox" href="{{ url('project/'.$project->id.'/view')}}">                
                                    <span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                @if(!empty($project->photos))
                                    @if(!empty($project->photos[0]))
                                        @if(!empty($project->photos[0]->filename))
                                            <img class = "img-responsive" src="{{asset(config('custom.photos_thumbnail_folder_www') . $project->photos[0]->filename)}}"  height = "{{ config('custom.photos_thumbnail_height') }}" width = "{{ config('custom.photos_thumbnail_width')}}" />
                                        @endif
                                    @endif
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @else
                    <h3>No projects available at this moment</h3>
                @endif
            </section>
        </div>
    </div>
@endsection
