@extends('frontend.index')
@section('content')
    <div class="span9">
        <div class="row">
            <section id="projects">
                @if(!empty($projects))
                    <ul id="thumbs">
                        <div class="col-xs-12 col-lg-12 col-md-12">
                            @foreach($projects as $project)
                                @if($project->type_id == 1)
                                    <li class="item-thumbs span3">
                                        <a class="hover-wrap fancybox" href="{{ url('project/'.$project->id.'/view')}}">                                   
                                            <span class="overlay-img"></span>
                                            <span class="overlay-img-thumb font-icon-plus"></span>
                                        </a>
                                        @if(!empty($project->photos))
                                            @if(!empty($project->photos[0]))
                                                @if(!empty($project->photos[0]->filename))
                                                    <img src="{{asset(config('custom.photos_thumbnail_folder_www') . $project->photos[0]->filename)}}" height = "{{ config('custom.photos_thumbnail_height') }}" width = "{{ config('custom.photos_thumbnail_width')}}" />
                                                @endif
                                            @endif
                                        @endif
                                    </li>
                                @elseif($project->type_id == 2)
                                    <li class="item-thumbs span3">
                                        <a class="hover-wrap fancybox" href="{{ url('project/'.$project->id.'/view')}}">
                                            <span class="overlay-img"></span>
                                            <span class="overlay-img-thumb font-icon-plus"></span>
                                        </a>
                                        @if(!empty($project->videos))
                                            @if(!empty($project->videos[0]))
                                                @if(!empty($project->videos[0]->url))
                                                    <img  src="http://img.youtube.com/vi/{{ $project->videos[0]->url }}/0.jpg" height = "{{ config('custom.photos_thumbnail_height') }}" width = "{{ config('custom.photos_thumbnail_width')}}"/>
                                                @endif
                                            @endif
                                        @endif
                                    </li>
                                @endif                  
                            @endforeach

                        </div>   
                    </ul>
                @else
                    <h3>No projects available at this moment</h3>
                @endif
            </section>            
        </div>
    </div>
@endsection
