@extends('frontend.index')
@section('content')
    @if(!empty($project->photos))
        <div class="span9">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-lg-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-lg-12">
                        @if(!empty($project->project_name))
                           <h3> {!! stripslashes($project->project_name) !!}</h3>
                        @endif
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-lg-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-lg-12">
                        @if(!empty($project->description))
                            {!! stripslashes($project->description) !!}
                        @endif
                    </div>
                </div>
                <br/>
                <br/>
                <section id="projects">
                    <ul id="thumbs">
                        @foreach($project->photos as $photo)
                            @if(!empty($photo->filename))
                                @if(!empty($photo->title))
                                    <?php $ti = $photo->title;?>
                                @else
                                    <?php $ti = "";?>
                                @endif
                                @if(!empty($photo->description))
                                    <?php $de = $photo->description;?>
                                @else                                        
                                     <?php $de ="";?>
                                @endif
    
                                <li class="item-thumbs span3">
                                    <a class="hover-wrap fancybox" data-fancybox-group="{{ $project->project_name }}" title="{{ $ti }}" href="{{asset(config('custom.photos_folder_www') . $photo->filename)}}">
                                        <span class="overlay-img"></span>
                                        <span class="overlay-img-thumb font-icon-plus"></span>
                                    </a>
                                    <img src="{{asset(config('custom.photos_thumbnail_folder_www') . $photo->filename)}}" alt="{{ $de}}" height = "{{ config('custom.photos_thumbnail_height') }}" width = "{{ config('custom.photos_thumbnail_width')}}">
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </section>
            </div>
        </div>
    @endif
@endsection