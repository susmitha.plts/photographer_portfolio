@extends('frontend.index')
    @section('content')
        @if(!empty($project->videos))
            <div class="span9">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-lg-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-lg-12">
                            @if(!empty($project->project_name))
                               <h3> {!! stripslashes($project->project_name) !!}</h3>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-lg-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-lg-12">
                            @if(!empty($project->description))
                                {!! stripslashes($project->description) !!}
                            @endif
                        </div>
                    </div><br/><br/>
                    <section id="projects">
                        <ul id="thumbs">
                            @foreach($project->videos as $video)
                                @if(!empty($video->url))
                                    @if(!empty($video->title))
                                        <?php $ti = $video->title;?>
                                    @else
                                        <?php $ti = "";?>
                                    @endif
                                    @if(!empty($video->description))
                                        <?php $de = $video->description;?>
                                    @else                                        
                                         <?php $de ="";?>
                                    @endif
    
                                    <li class="item-thumbs span3">
                                       <a class="hover-wrap fancybox-media" data-fancybox-group="{{ $project->project_name }}" title="{{ $ti }}" href="https://www.youtube.com/embed/{{ $video->url}}">
                                            <span class="overlay-img"></span>
                                            <span class="overlay-img-thumb font-icon-plus"></span>
                                        </a>
                                        @if(!empty($video->url))
                                            <img src="http://img.youtube.com/vi/{{ $video->url }}/0.jpg"/>
                                        @endif                                           
                                    </li>
                                @endif
                            @endforeach
                        </ul>            
                    </section>
                </div>
            </div>
        @endif
@endsection