@extends('frontend.index')
@section('content')
    <div class="span9">
        <div class="row">
            <section id="projects">
                @if(!empty($projects))
                    <ul id="thumbs">
                        @foreach($projects as $project)
                      
                        <li class="item-thumbs span3 ">
                            <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                            <a class="hover-wrap fancybox" href="{{ url('project/'.$project->id.'/view')}}">                              
                                <span class="overlay-img"></span>
                                <span class="overlay-img-thumb font-icon-plus"></span>
                            </a>
                                @if(!empty($project->videos))
                                    @if(!empty($project->videos[0]))
                                        @if(!empty($project->videos[0]->url))
                                            <img src="http://img.youtube.com/vi/{{ $project->videos[0]->url }}/0.jpg"/>
                                        @endif
                                    @endif
                                @endif
                        </li>
                        @endforeach                    
                    </ul>
                @else
                    <h3>No projects available at this moment</h3>
                @endif
            </section>
        </div>
    </div>
@endsection
        