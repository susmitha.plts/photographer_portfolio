@extends('admin.dashboard')
@section('section')
<div class="col-lg-12">
	<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">View Project</h2>
        </div>
    </div>
	<div class="row">  
		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">{{ $project->project_name }}  ( {{ $project->project_type->name}}  )</h3>
				</div>
				<div class="panel-body">
					<div class="row">  
						<div class="col-sm-12">
							@if(!empty($project->description))
								{!! nl2br(e($project->description)) !!}
							@endif
						</div>
					</div>
					<br/><br/><br/>
					<div class="row">	
						<div class="col-sm-12">
							@if($project->type_id == 1)
								@if(!empty($project->photos))
									@foreach($project->photos as $photo)
										@if(!empty($photo->filename))
					                    	<div class="col-xs-3 col-lg-3 col-md-3">
					                            <a target="_blank" href="{{asset(config('custom.photos_folder_www') . $photo->filename)}}" alt="cargo picture" title="cargo picture">
					                                <img src="{{asset(config('custom.photos_thumbnail_folder_www') . $photo->filename)}}" alt="cargo-photo" class="img-thumbnail">
					                            </a>
					                            <br/>
						                        @if(!empty($photo->original_file))
						                         	{{ str_limit($photo->original_file,15) }}
						                        @endif
					                        </div>
					                    @endif
					                @endforeach
					            @endif
					        @elseif($project->type_id == 2)
                                <div><h3><u>Videos</u></h3></div><br/><br/>
								@if(!empty($project->videos))
									<div class="col-xs-12 col-lg-12 col-md-12">	
										@foreach($project->videos as $video)
											@if(!empty($video->url))
						                        <iframe width="250" height="125" src="https://www.youtube.com/embed/{{ $video->url }}" frameborder="0" allowfullscreen></iframe>                   
						                        &nbsp;&nbsp;
						                    @endif
						                @endforeach
						            </div>	
					            @endif
					        @endif
						</div>
					</div>
				</div>
			</div>					
        </div>
	</div>
	<a href="{{ URL::to('admin/project/index') }}" class="btn btn-info btn-sm pull-left">Back to projects</a>
</div>
@endsection