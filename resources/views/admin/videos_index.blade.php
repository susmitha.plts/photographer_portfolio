@extends('admin.dashboard')
@section('section')
<div class="col-xs-12 clo-sm-12">
 @include ('partials.notifications')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Videos Index</h3>
		</div>
		<div class="panel-body">
			<input type = "hidden" name = "project_id" value = "{{ $project_id }}"/>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th  width="5%" class="hidden-480">#</th>
						<th class="hidden-480">Project name</th>
						<th>URL</th>
						<!--<th>Video</th>-->
						<th>Title</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@if($i=1)@endif
					@foreach ($videos as $video)
						<tr  class="table-tr-bg-{{{$video->id}}}">
							<td class="hidden-480">{{{ $video->id }}}</td>
							<td class="hidden-480">{{{ $video->project_name }}}</td>
							<td>@if(!empty($video->url)) {{ $video->url }} @endif</td>
							<!--<td>
							@if(!empty($video->url))
	                            
	                                <div class="col-xs-12 col-lg-12 col-md-12">
	                                    <iframe width="200" height="100" src="{{ $video->url }}" frameborder="0" allowfullscreen></iframe>
	                                </div>
	                            
	                        @endif
	                        </td>-->
	                        <td>{{  $video->title }}</td>
							<td>{{{ $video->updated_at }}}</td>
							<td>
				
								{!! Form::open(array('method'=> 'DELETE', 'route' => array('adminvideodestroy', $video->id))) !!}
								<button  data-placement="left" data-toggle="tooltip" title="Delete video" type="submit" class="btn btn-link btn-sm" onclick="return confirm('Do you want to delete this video ?')"><i class="fa fa-trash-o"></i></button>
								{!! Form::close() !!}
								<a data-toggle="tooltip"  class="btn btn-link btn-sm"title="Edit video details" href="{{URL::to('/admin/project/video/' . $video->id . '/edit')}}" ><i class="fa fa-pencil"></i></a>
								<a data-toggle="tooltip"  class="btn btn-link btn-sm" title="View project" href="{{URL::to('/admin/project/video/' . $video->id . '/view')}}" ><i class="fa fa-eye"></i></a>
								
							</td>
						</tr>
					@if($i++)@endif    
					@endforeach
				</tbody>
			</table>	
			{!! $videos->render() !!}		
		</div>
	</div>
	<a href="{{ URL::to('admin/project/index') }}" class="btn btn-info btn-sm pull-right">Back to projects</a>
</div>
@endsection