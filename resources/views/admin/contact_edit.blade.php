@extends('admin.dashboard')
@section('section')
	<div class="row">
		<div class="col-xs-12">

			@include ('partials.notifications')
	
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Edit Contact Detail</h4>
						</div>
						<div class="widget-body">
							<div class="widget-main no-padding">
								<form method="post" action="{{ url('admin/contact/update') }}" >

									<div class="widget-main">
										{!! csrf_field() !!}
										<label for="name">Type</label>
										<div class="row">
											<div class="col-xs-12 col-sm-12">
												<select class="form-control" name="settings_type" required>
													<option value="">--Select--</option>
													@foreach ($settings_types as $key=>$settings_type)
													<option value="{{ $key }}" <?php if($setting->type == $key){ echo "selected"; } ?> >{{ $settings_type }}</option>
													@endforeach
												</select>
												<input type="hidden" name="id" value="{{ $setting->id }}" />
											</div>
										</div>
										<br/>
										<div class="space space-8"></div>
										<label for="name">{{ ucfirst($setting->key) }}</label>
										<div class="row">
											<div class="col-xs-12 col-sm-12">
												<textarea class="form-control" rows="10" name="settings_value" placeholder="Settings Value"required>{{ Input::old('settings_value' , $setting->value ) }}</textarea>
											</div>
										</div>
									</div>
									<br/>
									<div class="widget-toolbox padding-8 clearfix">
										<a href="{{ URL::to('admin/contact/index') }}" class="btn btn-link pull-left">Cancel</a>
										<button type="submit" class="btn btn-sm btn-success pull-right">
											Update
											<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection