@extends('admin.dashboard')
@section('section')
	<div class="row">
		<div class="col-xs-12 clo-sm-12">

	        @include ('partials.notifications')

			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<h4>Add New Project</h4>
					<form method="post" action="{{ url('admin/project/photo/addpost') }}" enctype="multipart/form-data" >

						<div class="col-sm-12 col-xs-12">
							{!! csrf_field() !!}
							<input type = "hidden" name = "type_id" value = "{{$id}}"/>
							
							<label for="name">Project Title</label>
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<input type="text" class="form-control" name="title" placeholder="Project title" required />
								</div>
							</div><br/>
							<label for="name">Project Description</label>
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<textarea name ="description" rows ="10"class = "form-control" placeholder="Project description" required></textarea>
								</div>
							</div><br/>
							<label for="name">Upload Photos</label>
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									 <input type="file" name="files[]" id = "filer_input2" multiple="multiple">  
								</div>
							</div>
							<br/>
						</div>
						<br/>
						<div class="widget-toolbox padding-8 clearfix">
							<a href="{{ URL::to('admin/project/index') }}" class="btn btn-link pull-left">Cancel</a>
							<button type="submit" class="btn btn-sm btn-success pull-right">
								Submit
								<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

 
