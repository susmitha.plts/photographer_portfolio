@extends('admin.plane')
@section('body')
    <div class="container">
        <div class="row">
            @include('partials.notifications')
            <div class="col-md-4 col-md-offset-4"><br /><br /><br />
                <div class="panel panel-default">
	  		        <div class="panel-heading">
		              <h3 class="panel-title">Please Sign In</h3>
	                </div>
    	          	<div class="panel-body">
                        <form role="form" method = "post" action = "{{ url('admin/dologin') }}">
                            {!! csrf_field() !!}
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="username" type="email" autofocus/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value=""/>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="Login" />
                            </fieldset>
                        </form>
                	</div>
                </div>
            </div>
        </div>
    </div>
@endsection