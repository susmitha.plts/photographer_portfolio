@extends('admin.dashboard')
@section('section')
	<div class="col-xs-12 clo-sm-12">
	 	@include ('partials.notifications')
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Contacts Index</h3>
			</div>
			<div class="panel-body">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th  width="5%" class="hidden-480">#</th>
							<th class="hidden-480">Type</th>
							<th>Key</th>
							<th>Value</th>
							<th width="15%" class="hidden-480">Modified</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@if($i=1)@endif
						@foreach ($settings as $setting)
						<tr  class="table-tr-bg-{{{$setting->id}}}">
							<td class="hidden-480">{{{ $setting->id }}}</td>
							<td class="hidden-480">{{{ $setting->type }}}</td>
							<td>{{{ $setting->key }}}</td>
							<td>{{{ $setting->value }}}</td>
							<td class="hidden-480"><span class="date-time-field">{{ $setting->created_at }}</span></td>
							<td>
				
								{!! Form::open(array('method'=> 'DELETE', 'route' => array('admincontactdestroy', $setting->id))) !!}
								<button  data-placement="left" data-toggle="tooltip" title="Delete settings" type="submit" class="btn btn-link btn-sm" onclick="return confirm('Do you want to delete this contact detail ?')"><i class="fa fa-trash-o"></i></button>
								{!! Form::close() !!}
								<a data-toggle="tooltip"  class="btn btn-link btn-sm"title="Edit settings" href="{{URL::to('/admin/contact/' . $setting->id . '/edit')}}" ><i class="fa fa-pencil"></i></a>
								
							</td>
						</tr>
						@if($i++)@endif    
						@endforeach
					</tbody>
				</table>	
				{!! $settings->render() !!}		
			</div>
		</div>
		<a href="{{ URL::to('admin/contact/add') }}" class="btn btn-info btn-sm pull-right">Add new contact detail</a>
	</div>
@endsection