@extends('admin.dashboard')
@section('section')
<div class="col-xs-12 clo-sm-12">
 	@include ('partials.notifications')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Videos Index</h3>
		</div>
		<div class="panel-body">
			@if(!empty($videos))
				<table class="table table-bordered">
					<thead>
						<tr>
							<th  width="5%" class="hidden-480">#</th>
							<th class="hidden-480">Project name</th>
							<th>url</th>
							<th>Title</th>
							<th>Modified</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@if($i=1)@endif
						@foreach ($videos as $video)
							<tr class="table-tr-bg-{{{$video->id}}}">
								<td class="hidden-480">{{{ $video->id }}}</td>
								<td class="hidden-480">{{{ $video->project_name }}}</td>
								<td>
									@if(!empty($video->url))
			                           {{ $video->url }}
			                        @endif
	                        	</td>
	                        	<td>
		                        	@if(!empty($video->title))
		                        		{{  $video->title }}
		                        	@endif
	                        	</td>
								<td>{{{ $video->updated_at }}}</td>
								<td>				
									{!! Form::open(array('method'=> 'DELETE', 'route' => array('adminvideodestroy', $video->id))) !!}
										<button  data-placement="left" data-toggle="tooltip" title="Delete photo" type="submit" class="btn btn-link btn-sm" onclick="return confirm('Do you want to delete this video ?')"><i class="fa fa-trash-o"></i></button>
									{!! Form::close() !!}
									<a data-toggle="tooltip"  class="btn btn-link btn-sm"title="Edit photo details" href="{{URL::to('/admin/project/video/' . $video->id . '/edit')}}" ><i class="fa fa-pencil"></i></a>
								</td>
							</tr>
						@if($i++)@endif    
						@endforeach
					</tbody>
				</table>
				{!! $videos->render() !!}
			@endif	
            
			<label for="name">Add more videos to project</label>
			<form method="post" action="{{ url('admin/project/video/addmore') }}" enctype="multipart/form-data" >
				<input type = "hidden" name ="project_id" value = "{{ $project_id }}"/>
					{!! csrf_field() !!}
					<div class="row">
						<div class="col-xs-12 col-sm-12">   
				            <div class="input_fields_wrap">
				                <input type="text" name="video_links[]" id="video_links" class="form-control" required><br/>	            
				                <button class="add_field_button"><i class="fa fa-plus-square"></i></button>
				            </div>
				            <button type="submit" class ="btn btn-success pull-right">Submit</button>
				            <span class="error-span" id="error-hazardous_docs"></span>      
						</div>
					</div>
					<br/>
			</form>	
		</div>
	</div>
	<a href="{{ URL::to('admin/project/index') }}" class="btn btn-link pull-left">Back to projects</a>
</div>
@endsection
@section('javascript')
<script>
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><br/><input type="text" name="video_links[]" id="video_links" class="form-control" required /><a href="#" class="remove_field">Remove</a><br/></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })

</script>
@endsection
