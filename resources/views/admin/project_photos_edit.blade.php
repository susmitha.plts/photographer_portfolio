@extends('admin.dashboard')
@section('section')
	<div class="row">
		<div class="col-xs-12 clo-sm-12">

	        @include ('partials.notifications')

			<div class="row">
				<div class="col-sm-12 col-xs-12">
				<h4>Edit Project - {{ $project->project_name }}</h4>
			
					<form method="post" action="{{ url('admin/project/update') }}" enctype="multipart/form-data" >

						<div class="col-sm-12 col-xs-12">
							{!! csrf_field() !!}
							<input type = "hidden" id ="pro_id" name = "pro_id" value = "{{ $project->id }}"/>
							<label for="name">Project Title</label>
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<input type="text" id ="pro_name" class="form-control" name="title" placeholder="Project name" value = "{{ $project->project_name }}"required />
								</div>
							</div>
							<label for="name">Project Description</label>
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<textarea name ="description" rows ="10"class = "form-control" id = "pro_desc"required>{{  $project->description }}</textarea>
								</div>
							</div>
							<br/>
						</div>
						<br/>
						<div class="widget-toolbox padding-8 clearfix">
							<a href="{{ URL::to('admin/project/index') }}" class="btn btn-link pull-left">Cancel</a>
							<div class="pull-right">
								<button type="submit"  name = "submit_pro" class="btn btn-sm btn-success">
									Submit
									<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
								</button>&nbsp;&nbsp;&nbsp;
								@if($project->type_id == 1)
									<button type="submit" name = "submit_photo" class="btn btn-sm btn-success" name ="add_photos" id ="add_photos">
										Submit&amp;edit photos
										<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
									</button>
								@elseif($project->type_id == 2)
									<button type="submit" name = "submit_video" class="btn btn-sm btn-success" name ="add_videos" id ="add_videos">
										Submit&amp;edit videos
										<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
									</button>
								@endif
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection


 
