@extends('admin.dashboard')
@section('section')
<div class="col-lg-12">
	<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">View Project</h2>
        </div>
    </div>
	<div class="row">  
		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">@if(!empty($video->title)) {{ $video->title }} @endif</h3>
				</div>
				<div class="panel-body">
					<label>Title</label>
					<div class="row">  
						<div class="col-sm-12">
							@if(!empty($video->title))
								{!! stripslashes($video->title) !!}
							@endif
						</div>
					</div><br/>
					<label>Description</label>
					<div class="row">  
						<div class="col-sm-12">
							@if(!empty($video->description))
								{!! nl2br(e($video->description)) !!}
							@endif
						</div>
					</div>
					<br/><br/><br/>
					<div class="row">	
						<div class="col-xs-12 col-lg-12 col-md-12">								
							@if(!empty($video->url))
                    			<div class="col-xs-12 col-lg-12 col-md-12">
                         			<iframe width="250" height="125" src="https://www.youtube.com/embed/{{ $video->url }}" frameborder="0" allowfullscreen></iframe>
                                    <br/>
                                </div>
                            @endif		        
						</div>
					</div>
				</div>
			</div>	
			<a href="{{ URL::to('admin/project/video/'.$video->project_id.'/index') }}" class="btn btn-link pull-left">Back to videos index</a>
			<a href="{{ URL::to('admin/project/index') }}" class="btn btn-info btn-sm pull-right">Back to projects</a>				
        </div>
	</div>
</div>
@endsection