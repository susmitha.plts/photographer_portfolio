<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
	<head>
		<meta charset="utf-8"/>
		<title>Photographer Portfolio</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport"/>
		<meta content="" name="description"/>
		<meta content="" name="author"/>
		<link href="{{ asset('assets/admin/jq/css/jquery.filer.css')}}" type="text/css" rel="stylesheet" />
	    <link href="{{ asset('assets/admin/jq/css/themes/jquery.filer-dragdropbox-theme.css')}}" type="text/css" rel="stylesheet" />
	    <link rel="stylesheet" href="{{ asset('assets/admin/stylesheets/styles.css') }}" />
	    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	    <script type="text/javascript" src="{{ asset('assets/admin/scripts/frontend.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/ckeditor/ckeditor.js') }}"></script>   
	    
	    <script type="text/javascript" src="{{ asset('assets/admin/jq/js/jquery.filer.min.js?v=1.0.5')}}"></script>
	    <script type="text/javascript" src="{{ asset('assets/admin/jq/js/custom.js?v=1.0.5')}}"></script> 
	    
	</head>
	<body>
		@yield('body')
	    @yield('javascript')
	    @yield('styles')
	</body>

</html>