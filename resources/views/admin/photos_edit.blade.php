@extends('admin.dashboard')
@section('section')
    <h3>Edit- @if(!empty( $photo->project_name)) {{ $photo->project_name }} | {{ $photo->original_file}} @endif</h3>    
    <div class="col-xs-12">
        @include ('partials.notifications')
        <form method="post" action="{{ url('admin/project/photo/update') }}" enctype="multipart/form-data" >
            {!! csrf_field() !!}
            <input type = "hidden" name="photo_id" value="{{ $photo->id }}"/>
            <input type = "hidden" name="project_id" value="{{ $photo->project_id }}"/>
            <div class="row">
                @if(!empty($photo->filename))
                	<div class="col-xs-12 col-lg-12 col-md-12">
                        <a target="_blank" href="{{asset(config('custom.photos_folder_www') . $photo->filename)}}" alt="cargo picture" title="cargo picture">
                            <img src="{{asset(config('custom.photos_thumbnail_folder_www') . $photo->filename)}}" alt="cargo-photo" class="img-thumbnail">
                        </a>
                        <br/>
                        @if(!empty($photo->original_file))
                         	{{ $photo->original_file }}
                        @endif
                    </div>
                @endif
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-12">
                            <label>Change Photo</label>
                            <input type="file" name="files" id = "filer_input2"/>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <label class="font_copy col-lg-6 col-md-6 padding2">Title</label>
                @if(!empty($photo->title))
                    <div class="col-xs-12 col-lg-12 col-md-12">
                    <input type="text" class="form-control" name="title" placeholder="Photo title"  value="{{ $photo->title }}" required />
                    </div>
                @else
                    <div class="col-xs-12 col-lg-12 col-md-12">
                       <input type="text" class="form-control" name="title" placeholder="Photo title" required />
                    </div>
                @endif
            </div>
            <br/>
            <div class="row">
                <label class="font_copy col-lg-6 col-md-6 padding2">Description</label>
                @if(!empty($photo->description))
                    <div class="col-xs-12 col-lg-12 col-md-12">
                        <textarea name ="description" rows ="10"class = "form-control" required>{{ $photo->description }}</textarea>
                    </div>
                @else
                    <div class="col-xs-12 col-lg-12 col-md-12">
                        <textarea name ="description" rows ="10"class = "form-control" required></textarea>
                    </div>
                @endif
            </div>
            <br/>
            <div class="widget-toolbox padding-8 clearfix">
				<a href="{{ URL::to('admin/project/photo/'.$photo->project_id.'/index') }}" class="btn btn-link pull-left">Cancel</a>
				<button type="submit" class="btn btn-sm btn-success pull-right">
					Submit
				    <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
				</button>
			</div>
        </form>
    </div>               
@endsection