@extends('admin.dashboard')
@section('section')
<div class="col-xs-12 clo-sm-12">
 	@include ('partials.notifications')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Projects Index</h3>
		</div>
		<div class="panel-body">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="5%" class="hidden-480">#</th>
						<th class="hidden-480">Project type</th>
						<th>Project name</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@if($i=1)@endif
					@foreach ($projects as $project)
						<tr class="table-tr-bg-{{{$project->id}}}">
							<td class="hidden-480">{{{ $project->id }}}</td>
							<td class="hidden-480">{{{ $project->name }}}</td>
							<td class="hidden-480">{{{ $project->project_name }}}</td>
							<td>{{{ $project->updated_at }}}</td>
							<td>
								{!! Form::open(array('method'=> 'DELETE', 'route' => array('adminprojectdestroy', $project->id))) !!}
									<button  data-placement="left" data-toggle="tooltip" title="Delete photo" type="submit" class="btn btn-link btn-sm" onclick="return confirm('Do you want to delete this project ?')"><i class="fa fa-trash-o"></i></button>
								{!! Form::close() !!}
								<a data-toggle="tooltip"  class="btn btn-link btn-sm"title="Edit photo details" href="{{URL::to('/admin/project/' . $project->id . '/edit')}}" ><i class="fa fa-pencil"></i></a>
								<a data-toggle="tooltip"  class="btn btn-link btn-sm" title="View project" href="{{URL::to('/admin/projects/' . $project->id . '/view')}}" ><i class="fa fa-eye"></i></a>
								@if($project->type_id == 1)
									<a data-toggle="tooltip"  class="btn btn-link btn-sm" title="View photos in project" href="{{URL::to('/admin/project/photo/' . $project->id . '/index')}}" ><i class="fa fa-picture-o"></i></a>
								@elseif($project->type_id == 2)
									<a data-toggle="tooltip"  class="btn btn-link btn-sm" title="View videos in project" href="{{URL::to('/admin/project/video/' . $project->id . '/index')}}" ><i class="fa fa-video-camera"></i></a>
								@endif
							</td>
						</tr>
					@if($i++)@endif    
					@endforeach
				</tbody>
			</table>	
			{!! $projects->render() !!}		
		</div>
	</div>
	<a href="{{ URL::to('admin/project/add') }}" class="btn btn-info btn-sm pull-right">Add new project</a>
</div>
@endsection