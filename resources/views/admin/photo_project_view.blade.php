@extends('admin.dashboard')
@section('section')
	<div class="col-lg-12">
		<div class="row">
	        <div class="col-lg-12">
	            <h2 class="page-header">View Project</h2>
	        </div>
	    </div>
		<div class="row">  
			<div class="col-sm-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">@if(!empty($photo->title)) {{ $photo->title }} @endif</h3>
					</div>
					<div class="panel-body">
						<label>Title</label>
						<div class="row">  
							<div class="col-sm-12">
								@if(!empty($photo->title))
									{!! stripslashes($photo->title) !!}
								@endif
							</div>
						</div><br/>
						<label>Description</label>
						<div class="row">  
							<div class="col-sm-12">
								@if(!empty($photo->description))
									{!! nl2br(e($photo->description)) !!}
								@endif
							</div>
						</div>
						<br/><br/><br/>
						<div class="row">	
							<div class="col-sm-12">								
								@if(!empty($photo->filename))
			                    	<div class="col-xs-3 col-lg-3 col-md-3">
			                            <a target="_blank" href="{{asset(config('custom.photos_folder_www') . $photo->filename)}}" alt="cargo picture" title="cargo picture">
			                                <img src="{{asset(config('custom.photos_thumbnail_folder_www') . $photo->filename)}}" alt="cargo-photo" class="img-thumbnail">
			                            </a>
			                            <br/>
			                        @if(!empty($photo->original_file))
			                         	{{ $photo->original_file }}
			                        @endif
			                        </div>
			                    @endif					        
							</div>
						</div>
					</div>
				</div>					
	        </div>
		</div>
		<a href="{{ URL::to('admin/project/photo/'.$photo->project_id.'/index') }}" class="btn btn-link pull-left">Back to photo index</a>
		<a href="{{ URL::to('admin/project/index') }}" class="btn btn-info btn-sm pull-right">Back to projects</a>
	</div>
@endsection