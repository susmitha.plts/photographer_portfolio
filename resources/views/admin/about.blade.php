@extends('admin.dashboard')
@section('section')
<div class="col-xs-12 col-sm-12">
	<div class="widget-box">
		<div class="widget-header">
			<h4 class="widget-title">Edit {{$content->title}}</h4>
		</div>
		<div class="widget-body">
		 @include ('partials.notifications')
			<div class="widget-main no-padding">
				<form method="post" action="{{ url('admin/about/' . $content->id .  '/update') }}" enctype='multipart/form-data'>
					<!-- <legend>Form</legend> -->
					<div class="widget-main">
						{!! csrf_field() !!}
						<label for="title">Title</label>
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<input type="text" required="required" class="form-control" name="title" placeholder="Content title" value="{{ Input::old('title' , $content->title ) }}" required />
								<input type="hidden" name="id" value="{{ $content->id }}" />
							</div>
						</div>
                        <br/>
						<label for="meta_title">Meta title</label>
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<input type="text" required="required" class="form-control" name="meta_title" id="meta_title" placeholder="SEO meta title" value="{{ Input::old('meta_title' , $content->meta_title ) }}" required />
								
							</div>
						</div>
						<br/>
						<div class="space space-8"></div>
						<label for="keywords">Meta keywords</label>
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<input type="text" class="form-control" name="keywords" placeholder="SEO keywords" value="{{ Input::old('keywords', $content->keywords ) }}" />
							</div>
						</div>
						<br/>
						<div class="space space-8"></div>
						<label for="meta_description">Meta descrption</label>
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<textarea name="meta_description" id="meta_description" class="form-control">{{ Input::old('meta_description' , $content->meta_description ) }}</textarea>
							</div>
						</div>
						<br/>
						<div class="space space-8"></div>
						<label for="content">Content</label>
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<textarea id="description"  class="form-control ckeditor" required="required"  name="content" placeholder="Content">{{ Input::old('content' , $content->content ) }}</textarea>
							</div>
						</div>
					</div>
					<div class="space space-8"></div>
					<br/>
					<div class="widget-toolbox padding-8 clearfix">
						<a href="{{ URL::to('admin/dashboard') }}" class="btn btn-link pull-left">Cancel</a>
						<button type="submit" class="btn btn-sm btn-success pull-right">Update
							<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection