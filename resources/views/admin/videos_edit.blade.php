@extends('admin.dashboard')
@section('section')
    <div class="col-xs-12">
        <h3>Edit- @if(!empty( $video->project_name)) {{ $video->project_name }}  @endif @if(!empty( $video->title))|{{ $video->title }} @endif</h3>    
    </div>  
    <div class="col-xs-12">
        @include ('partials.notifications')
        <form method="post" action="{{ url('admin/project/video/update') }}" enctype="multipart/form-data" >
            {!! csrf_field() !!}
            <input type = "hidden" name="video_id" value="{{ $video->id }}"/>
            <input type = "hidden" name="project_id" value="{{ $video->project_id }}">
            <div class="row">
                @if(!empty($video->url))
            	   <div class="col-xs-12 col-lg-12 col-md-12">
                        <iframe width="300" height="150" src="https://www.youtube.com/embed/{{ $video->url }}" frameborder="0" allowfullscreen></iframe>   
                        <br/>
                    </div>
                @endif
            </div>
            <br/>
            <label>Change video</label><br/>
            <div class="row">
                <div class="col-xs-12 col-lg-12 col-md-12">
                    <input type = "text" name = "change_video" id ="change_video" class="form-control"/>
                </div>
            </div>
            <br/>
            <div class="row">
                <label class="font_copy col-lg-6 col-md-6 padding2">Title</label>
                @if(!empty($video->title))
                    <div class="col-xs-12 col-lg-12 col-md-12">
                    <input type="text" class="form-control" name="title" placeholder="Video title"  value="{{ $video->title }}" required />
                    </div>
                @else
                    <div class="col-xs-12 col-lg-12 col-md-12">
                       <input type="text" class="form-control" name="title" placeholder="Video title" required />
                    </div>
                @endif
            </div>
            <br/>
            <div class="row">
                <label class="font_copy col-lg-6 col-md-6 padding2">Description</label>
                @if(!empty($video->description))
                    <div class="col-xs-12 col-lg-12 col-md-12">
                        <textarea name ="description" rows ="10"class = "form-control"  placeholder="Video description" required>{{ $video->description }}</textarea>
                    </div>
                @else
                    <div class="col-xs-12 col-lg-12 col-md-12">
                        <textarea name ="description" rows ="10"class = "form-control" placeholder="Video description" required></textarea>
                    </div>
                @endif
            </div>
            <br/>
            <div class="widget-toolbox padding-8 clearfix">
        		<a href="{{ URL::to('admin/project/video/'.$video->project_id.'/index') }}" class="btn btn-link pull-left">Cancel</a>
        		<button type="submit" class="btn btn-sm btn-success pull-right">
        			Submit
        		    <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
        		</button>
        	</div>
        </form>
    </div>                  
@endsection