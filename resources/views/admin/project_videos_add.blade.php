@extends('admin.dashboard')
@section('section')
	<div class="row">
		<div class="col-xs-12 clo-sm-12">
	        @include ('partials.notifications')
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<h4>Add New Project</h4>
					<form method="post" action="{{ url('admin/project/video/addpost') }}" enctype="multipart/form-data" >
						<!-- <legend>Form</legend> -->
						<div class="col-sm-12 col-xs-12">
							{!! csrf_field() !!}
							<input type = "hidden" name = "type_id" value = "{{$id}}"/>
							<label for="name">Project Title</label>
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<input type="text" class="form-control" name="title" placeholder="Project title" required />
								</div>
							</div><br/>
							<label for="name">Project Description</label>
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<textarea name ="description" rows ="10"class = "form-control" placeholder="Project description"required></textarea>
								</div>
							</div><br/>
							<label for="name">Add Videos</label>
							<div class="row">
								<div class="col-xs-12 col-sm-12">   
						            <div class="input_fields_wrap">
						                <input type="text" name="video_links[]" id="video_links" class="form-control" required><br/>	            
						                <button class="add_field_button"><i class="fa fa-plus-square"></i></button>
						            </div>   
	   							</div>
							</div>
							<br/>
						</div>
						<br/>
						<div class="widget-toolbox padding-8 clearfix">
							<a href="{{ URL::to('admin/contact/index') }}" class="btn btn-link pull-left">Cancel</a>
							<button type="submit" class="btn btn-sm btn-success pull-right">
								Submit
								<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('javascript')
	<script>
	 	var max_fields      = 10; //maximum input boxes allowed
	    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
	    var add_button      = $(".add_field_button"); //Add button ID
	   
	    var x = 1; //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div><br/><input type="text" name="video_links[]" id="video_links" class="form-control" required /><a href="#" class="remove_field">Remove</a><br/></div>'); //add input box
	        }
	    });
	   
	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    })

	</script>
@endsection

 