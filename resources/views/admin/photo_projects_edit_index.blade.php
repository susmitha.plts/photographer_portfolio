@extends('admin.dashboard')
@section('section')
	<div class="col-xs-12 clo-sm-12">
	 	@include ('partials.notifications')
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Photos Index</h3>
			</div>
			<div class="panel-body">
				@if(!empty($photos))
					<table class="table table-bordered">
						<thead>
							<tr>
								<th  width="5%" class="hidden-480">#</th>
								<th class="hidden-480">Project name</th>
								<th>File</th>
								<th>Title</th>
								<th>Modified</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@if($i=1)@endif
							@foreach ($photos as $photo)
								<tr  class="table-tr-bg-{{{$photo->id}}}">
									<td class="hidden-480">{{{ $photo->id }}}</td>
									<td class="hidden-480">{{{ $photo->project_name }}}</td>
									<td>
									@if(!empty($photo->filename))
			                            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12" >
			                                <div class="col-xs-12 col-lg-12 col-md-12">
			                                    <a target="_blank" href="{{asset(config('custom.photos_folder_www') . $photo->filename)}}" alt="cargo picture" title="cargo picture">
			                                        <img src="{{asset(config('custom.photos_thumbnail_folder_www') . $photo->filename)}}" alt="cargo-photo" class="img-thumbnail">
			                                    </a>
			                                </div>
			                            </div>
			                        @endif
			                        </td>
			                        <td>{{  $photo->title }}</td>
									<td>{{{ $photo->updated_at }}}</td>
									<td>				
										{!! Form::open(array('method'=> 'DELETE', 'route' => array('adminphotodestroy', $photo->id))) !!}
										<button  data-placement="left" data-toggle="tooltip" title="Delete photo" type="submit" class="btn btn-link btn-sm" onclick="return confirm('Do you want to delete this photo ?')"><i class="fa fa-trash-o"></i></button>
										{!! Form::close() !!}
										<a data-toggle="tooltip"  class="btn btn-link btn-sm"title="Edit photo details" href="{{URL::to('/admin/project/photos/' . $photo->id . '/edit')}}" ><i class="fa fa-pencil"></i></a>
										<a data-toggle="tooltip"  class="btn btn-link btn-sm" title="View project" href="{{URL::to('/admin/projects/photos/' . $photo->id . '/view')}}" ><i class="fa fa-eye"></i></a>
										
									</td>
								</tr>
							@if($i++)@endif    
							@endforeach
						</tbody>
					</table>
				@endif	
				{!! $photos->render() !!}
	            
				<label for="name">Add more photos to project</label>
				<form method="post" action="{{ url('admin/project/photo/addmore') }}" enctype="multipart/form-data" >
					<input type = "hidden" name ="project_id" value = "{{ $project_id }}"/>
					{!! csrf_field() !!}
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							 <input type="file" name="files[]" id = "filer_input2" multiple="multiple" />
							    <button type="submit" class="btn btn-sm btn-success pull-right">
									Submit
									<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
							    </button> 
						</div>
					</div>	
				</form>	
			</div>
		</div>
		<a href="{{ URL::to('admin/project/index') }}" class="btn btn-link pull-left">Back to projects</a>
	</div>
@endsection
