@extends('admin.dashboard')
@section('section')
<div class="row">
	<div class="col-xs-12 clo-sm-12">
        @include ('partials.notifications')
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title">Add Contact Detail</h4>
					</div>
					<div class="widget-body">
						<div class="widget-main no-padding">
							<form method="post" action="{{ url('admin/contact/addpost') }}" >
								<div class="widget-main">
									{!! csrf_field() !!}
									<label for="name">Type</label>
									<div class="row">
										<div class="col-xs-12 col-sm-12">
											<select class="form-control" name="settings_type" required>
												<option value="">--Select--</option>
												@foreach ($settings_types as $key=>$settings_type)
												<option value="{{ $key }}" >{{ $settings_type }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<label for="name">Key</label>
									<div class="row">
										<div class="col-xs-12 col-sm-12">
											<input type="text" class="form-control" name="key" placeholder="Settings Key" required />
										</div>
									</div>
									<label for="name">Value</label>
									<div class="row">
										<div class="col-xs-12 col-sm-12">
											<textarea  class="form-control" rows="10" name="settings_value" placeholder="Settings Value" required></textarea>
										</div>
									</div>
								</div>
								<br/>
								<div class="widget-toolbox padding-8 clearfix">
									<a href="{{ URL::to('admin/contact/index') }}" class="btn btn-link pull-left">Cancel</a>
									<button type="submit" class="btn btn-sm btn-success pull-right">
										Submit
										<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
@endsection