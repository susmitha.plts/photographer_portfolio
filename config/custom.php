<?php
$path = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;
return [
	// User profile picture settings
    'photos_folder' 				=> trim($path . 'project_photos' . DIRECTORY_SEPARATOR),
    'photos_thumbnail_folder' 		=> trim($path . 'project_photos' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR),
    'photos_folder_www' 			    => "/uploads/project_photos/",
    'photos_thumbnail_folder_www' 	    => "/uploads/project_photos/thumbnails/",
    'photos_full_height' 			    => '500',
    'photos_full_width' 			    => '500',
    'photos_thumbnail_height' 		    => '150',
    'photos_thumbnail_width' 		    => '150',
    // Settings type
    'settings_type'                     => array("address" => "address", "phone" => "phone", "skype" => "skype", "email" => "email"),
];
